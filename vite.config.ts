import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";
import fs from "fs/promises";
import { JSDOM } from "jsdom";
import _upperFirst from "lodash/upperFirst";
import _camelCase from "lodash/camelCase";
import dts from "vite-plugin-dts";

const iconsPath = "./iconoir/icons/";
const outputPath = "./src/icons/";

const childElementsToArrayOfString = (svgEl: SVGSVGElement): string[] => {
  const childrenElements: string[] = [];
  for (let o = 0; o < svgEl.children.length; o++) {
    if (svgEl.children[o].hasAttribute("stroke")) {
      svgEl.children[o].setAttribute("stroke", "currentColor");
    }
    childrenElements.push(svgEl.children[o].outerHTML);
  }

  return childrenElements;
};

const buildIconComponent = (elements: string[]): string => {
  return `
    <template>
      <svg
        :width="size?.trim()"
        :height="size?.trim()"
        class="iconoir-icon"
        viewBox="0 0 24 24"
        stroke-width="1.5"
        fill="none"
        :color="color"
      >
        ${elements.join("")}
      </svg>
    </template>
              
    <script lang="ts" setup>
      defineProps({
        size: { type: String, default: '100%' },
        color: { type: String, default: "currentColor" },
      })
    </script>
  `;
};

const buildIconsRow = (fileName: string): string => {
  const icon = fileName.replace(".vue", "").replace("icon-", "");
  return `'${icon}': ${buildComponentName(fileName)}`;
};

// const buildIconsRow = (fileName: string): string => {
//   const icon = fileName.replace(".vue", "").replace("icon-", "");
//   return `'${icon}': () => import('./icons/${fileName}')`;
// };

const writeIconComponent = async (
  source: string,
  svgEl: SVGSVGElement,
): Promise<string> => {
  const fileName = "icon-" + source.replace("svg", "vue");
  const path = outputPath + fileName;
  console.log(`output ${path}`);
  await fs.writeFile(
    path,
    buildIconComponent(childElementsToArrayOfString(svgEl)),
  );

  return fileName;
};

const getSvgElementFromSource = async (
  source: string,
): Promise<SVGSVGElement> => {
  const content = await fs.readFile(iconsPath + source);
  const { window } = new JSDOM(content.toString());
  return window?.document?.querySelector("svg");
};

const buildCamelCaseIconName = (fileName: string): string => {
  return _camelCase(fileName.replace(".vue", ""));
};

const buildComponentName = (fileName: string): string => {
  return _upperFirst(buildCamelCaseIconName(fileName));
};

// const writeIndexFile = async (vueFiles: string[]) => {
//   await fs.writeFile(
//     "./src/index.ts",
//     `
//                 ${vueFiles
//                   .map(
//                     (i) =>
//                       `export const ${buildComponentName(
//                         i,
//                       )} = () => import('./icons/${i}')`,
//                   )
//                   .join(";\n")}
//
//                 export type IconoirIcon = '${vueFiles
//                   .map((i) => i.replace(".vue", "").replace("icon-", ""))
//                   .join("'|'")}';
//                                 `,
//   );
// };

const writeIndexFile = async (vueFiles: string[]) => {
  await fs.writeFile(
    "./src/index.ts",
    `
                ${vueFiles
                  .map(
                    (i) =>
                      `import ${buildComponentName(i)} from './icons/${i}'`,
                  )
                  .join(";\n")}

                export type IconoirIcon = '${vueFiles
                  .map((i) => i.replace(".vue", "").replace("icon-", ""))
                  .join("'|'")}';

                export {
                  ${vueFiles.map((i) => buildComponentName(i)).join(",\n")}
                }
                `,
  );
};

function buildIcons() {
  return {
    name: "build-icons",
    async buildStart() {
      const files = await fs.readdir(iconsPath);
      const vueFiles: string[] = [];

      try {
        for (let i = 0; i < files.length; i++) {
          const svgEl = await getSvgElementFromSource(files[i]);
          if (svgEl) {
            const fileName = await writeIconComponent(files[i], svgEl);
            vueFiles.push(fileName);
          }
        }

        await writeIndexFile(vueFiles);
      } catch (error) {
        console.log(error);
      }
    },
  };
}

export default defineConfig({
  plugins: [
    vue(),
    buildIcons(),
    dts({
      insertTypesEntry: true,
    }),
  ],
  build: {
    lib: {
      entry: resolve(__dirname, "src/index.ts"),
      name: "VueIconoir",
      fileName: "vue-iconoir",
    },
    rollupOptions: {
      external: ["vue"],
      output: {
        globals: {
          vue: "Vue",
        },
      },
    },
  },
});
